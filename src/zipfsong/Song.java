package zipfsong;

public class Song {
    private Integer track;
    private Integer listens;
    private String name;

    public Song(Integer track, Integer listens, String name) {
        this.track = track;
        this.listens = listens;
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        Song other = (Song)o;
        return this.track == other.track && this.listens == other.listens && this.name.equals(other.name);
    }

    public Integer quality() {
        return listens * track;
    }

    public String name() {
        return name;
    }
}
