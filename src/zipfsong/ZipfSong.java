package zipfsong;

import com.google.common.base.Joiner;

public class ZipfSong {

    private FileReader reader;

    public ZipfSong(FileReader reader) {
        this.reader = reader;
    }

    public String select() {
        Album album = reader.read();
        return Joiner.on("\n").join(album.orderDecreasing());
    }
}

