package zipfsong;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileReader {

    private String filename;

    public FileReader(String filename) {
        this.filename = filename;
    }

    public Album read() {
        Scanner scanner = null;
        Album album = null;
        try {
            scanner = new Scanner(new File(filename));
            while(scanner.hasNext()) {
                Integer numberOfSongs = Integer.valueOf(scanner.next());
                Integer numberOfSongsToSelect = Integer.valueOf(scanner.next());
                album = new Album(numberOfSongsToSelect);
                for (int i = 0; i < numberOfSongs; i++) {
                    Integer numberOfTimesListenedTo = Integer.valueOf(scanner.next());
                    String nameOfSong = scanner.next();
                    album.add(new Song(i+1, numberOfTimesListenedTo,  nameOfSong));
                }
            }
        } catch (FileNotFoundException e) {
            album = Album.EmptyAlbum;
        }
        return album;
    }
}