package zipfsong;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import com.google.common.primitives.Ints;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Album {
    public static Album EmptyAlbum = new Album(0){
        @Override
        public Integer numberOfSongs() {
            return 0;
        }
    };
    private List<Song> songs;
    private Integer numberOfSongsToSelect;

    public Album(Integer numberOfSongsToSelect) {
        this.numberOfSongsToSelect = numberOfSongsToSelect;
        this.songs = new ArrayList<Song>();
    }

    public Integer numberOfSongs() {
        return songs.size();
    }

    public void add(Song song) {
        songs.add(song);
    }

    public List<String> orderDecreasing() {
        Comparator<Song> byQuality = new Comparator<Song>() {
            @Override
            public int compare(Song song1, Song song2) {
                return Ints.compare(song1.quality(), song2.quality());
            }
        };
        List<Song> orderedSongs = Ordering.from(byQuality).reverse().sortedCopy(songs).subList(0, numberOfSongsToSelect);
        List<String> orderedSongNames = Lists.transform(orderedSongs, new Function<Song, String>() {
            @Override
            public String apply(Song song) {
                return song.name();
            }
        });

        return orderedSongNames;
    }
}
