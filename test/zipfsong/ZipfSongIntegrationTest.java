package zipfsong;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class ZipfSongIntegrationTest {

    @Test
    public void shouldProduceOutput1() {
        ZipfSong zipfSong = new ZipfSong(new FileReader("input1.txt"));
        assertThat(zipfSong.select(), is("four\ntwo"));
    }

    @Test
    public void shouldProduceOutput2() {
        ZipfSong zipfSong = new ZipfSong(new FileReader("input2.txt"));
        assertThat(zipfSong.select(), is("19_2000\nclint_eastwood\ntomorrow_comes_today"));
    }
}
