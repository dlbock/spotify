package zipfsong;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class AlbumTest {

    private Song four;
    private Song three;
    private Song two;
    private Song one;

    @Before
    public void setup() {
        one = new Song(1, 30, "one");
        two = new Song(2, 30, "two");
        three = new Song(3, 15, "three");
        four = new Song(4, 25, "four");
    }

    @Test
    public void shouldDisplaySongsInDecreasingOrderOfQuality() {
        Album album = createAlbum(2);
        List<String> actualSongs = album.orderDecreasing();
        List<String> expectedSongs = newArrayList("four", "two");
        assertThat(actualSongs, is(expectedSongs));
    }

    private Album createAlbum(int numberOfSongsToSelect) {
        Album album = new Album(numberOfSongsToSelect);
        album.add(one);
        album.add(two);
        album.add(three);
        album.add(four);
        return album;
    }
}
