package zipfsong;

import org.junit.Test;

import java.io.FileNotFoundException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class FileReaderIntegrationTest {

    private FileReader fileReader;

    @Test
    public void shouldCreateAnAlbumWithTheSpecifiedNumberOfSongs() throws FileNotFoundException {
        fileReader = new FileReader("input1.txt");
        Album album = fileReader.read();
        assertThat(album.numberOfSongs(), is(4));
    }

    @Test
    public void shouldReturnEmptyAlbumIfFileIsNotFound() throws FileNotFoundException {
        fileReader = new FileReader("nonexistentfile.txt");
        Album album = fileReader.read();
        assertThat(album.numberOfSongs(), is(0));
    }
}
