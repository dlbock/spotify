package zipfsong;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class SongTest {
    
    @Test
    public void shouldCalculateASongsZipfNumber() {
        Song song = new Song(3, 30, "foo");
        assertThat(song.quality(), is(90));
    }
}
